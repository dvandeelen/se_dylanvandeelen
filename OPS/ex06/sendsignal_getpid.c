#include <stdio.h>   // printf, getchar
#include <signal.h>  // sigaction, sigemptyset, struct sigaction, SIGINT
#include <stdlib.h>  // atoi
#include <unistd.h>  // sleep, write
#include <sys/stat.h>
#include <string.h>
#include <fcntl.h>

int main(int argc, char *argv[]) {

   int fd;
   int message;
   pid_t pid;
   
   fd = open("PIDpipe", O_RDONLY) ;
   read(fd, &message, sizeof(message)) ;
   close(fd);
   pid = message;
   printf("%d\n", pid);

   while(1)
   {
      kill(pid, 25);
      sleep(3);
   }
}
