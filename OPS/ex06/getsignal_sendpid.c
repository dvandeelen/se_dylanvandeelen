#define _POSIX_C_SOURCE 199309L
#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

char counter = '0';
void newHandler(int sig, siginfo_t *info, void *ucontext);

int main() 
{
  struct sigaction act, oldact;
  memset(&act, '\0', sizeof(act)); 	// Fill act with NULLs by default
  act.sa_sigaction = newHandler; 	// Set the custom SHR
  act.sa_flags = SA_SIGINFO; 		// No flags, used with act.sa_handler
  sigemptyset(&act.sa_mask);		// No signal masking during SHR execution
  sigaction(SIGXFSZ, &act, &oldact);	// Install SHR
  
  int fd;
  long pid = getpid();
  
  mkfifo("PIDpipe", S_IFIFO|0666);
  fd = open("PIDpipe", O_WRONLY);
  write (fd, &pid, sizeof(pid));
  close (fd);

  while(1){
    if(counter>'9'){
      counter='0';
    }
    write(1, &counter, 1);
    sleep(1);
  }
  sigaction(SIGXFSZ, &oldact, NULL);	// Restore original SHR:
  return 0;
}

void newHandler(int sig, siginfo_t *info, void *ucontect){
  counter++;
}
