#define _POSIX_C_SOURCE 199309L  // sigaction(), struct sigaction, sigemptyset()
#include <stdio.h>   // printf
#include <unistd.h>  // alarm()
#include <string.h>  // memset
#include <signal.h>  // sigaction, sigemptyset, struct sigaction, SIGINT

void myAlarm(int sig);
volatile int state=0;

int main(void) {
  struct sigaction act;
  
  // Define and install SHR:
  memset(&act, '\0', sizeof(act));
  act.sa_handler = myAlarm;
  act.sa_flags = 0;
  sigemptyset(&act.sa_mask);
  sigaction(SIGALRM, &act, NULL);
  
  printf("Setting alarm to go off in three seconds' time...\n");
  alarm(3);  // Schedule a SIGALRM signal to myself in three seconds
  
  printf("Looping forever...\n");
  while(state==0);
  
  printf("Alarm went off!\n");
  return 0;
}

void myAlarm(int sig) {
  printf("Received signal %i\n", sig);
  state = 1;
}
