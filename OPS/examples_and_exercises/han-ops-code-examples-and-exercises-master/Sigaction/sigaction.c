

/* Example of using sigaction() to setup a signal handler with 3 arguments
 * including siginfo_t.
 */
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>

char buf[20];
size_t nbytes;
ssize_t bytes_written;
int fd;
 
static void hdl (int sig, siginfo_t *siginfo, void *context)
{
	printf ("Sending PID: %ld, UID: %ld\n",
			(long)siginfo->si_pid, (long)siginfo->si_uid);
}
 
int main (int argc, char *argv[])
{
	printf("Hallo\n");
	
	strcpy(buf, "This is a test\n");
	nbytes = strlen(buf);
	bytes_written = write(fd, buf, nbytes);
	
	strcpy(buf, "This is a test2\n");
	bytes_written = write(fd, buf, nbytes);
	
	struct sigaction act;
 
	memset (&act, '\0', sizeof(act));
 
	/* Use the sa_sigaction field because the handles has two additional parameters */
	act.sa_sigaction = &hdl;
 
	/* The SA_SIGINFO flag tells sigaction() to use the sa_sigaction field, not sa_handler. */
	act.sa_flags = SA_SIGINFO;
 
	if (sigaction(SIGTERM, &act, NULL) < 0) {
		perror ("sigaction");
		return 1;
	}
 
	while (1)
		sleep (10);
		
 
	return 0;
}

