/*!
 * \brief Example installing a SHR using sigaction(), Mehtod 1.
 * This example will replace the existing sighandler for ctrl-c for a new one and
 * restore it when exiting the program.
 * 
 * Intital version by Marc Versluys
 * Updated version by Remko Welling, contains aditional code for explanation.
 * 
 * Program operation
 * -----------------
 * After starting the program the SHR executed after [ctrl-C] is installed
 * The new SHR will print to STDOUT when [ctrl-C] is detected until a [enter] is
 * received.
 * When enter is received the total number of times [ctrl-C] is detected is printed
 * Than the old signal handler is restored and [ctrl-C] is used to quit the program.
 */

#define _POSIX_C_SOURCE 199309L  // sigaction(), struct sigaction, sigemptyset()
#include <stdio.h>   // printf, getchar
#include <string.h>  // memset
#include <signal.h>  // sigaction, sigemptyset, struct sigaction, SIGINT
#include <unistd.h>	//sleep function
#include <sys/types.h>

volatile sig_atomic_t signalCount = 0;
void newHandler(int sig);
void writeFunction(char buf[20]); 
int y;
size_t nbytes;
ssize_t bytes_written;
int fd;

int main(void) {
  writeFunction("Hallo past deze tekst erin\n");
  struct sigaction act;     // Struct to store new signal handler
  struct sigaction oldact;  // Struct to store old signal handler that will be replaced.
   writeFunction("Hallo\n");

  // Define SHR:
  memset(&act, '\0', sizeof(act));  // Fill act with NULLs by default
  act.sa_handler = newHandler;      // Set the custom SHR
  act.sa_flags = 0;                 // No flags, used with act.sa_handler
  sigemptyset(&act.sa_mask);        // No signal masking during SHR execution 
  
  // Install SHR:
  sigaction(SIGUSR1, &act, &oldact);  // SIGINT = Ctrl-C, This cannot be SIGKILL or SIGSTOP
  
  while(1) {
  raise(SIGUSR1);
  }
    
  // Restore original SHR:
  sigaction(SIGUSR1, &oldact, NULL);
  
  return 0;
}

// SHR using sa_handler:
void newHandler(int sig) {
  //printf("  --  Signal caught: %d\n", sig);
  signalCount++;
   {
  	for (int i =0; i < 5; i++)
  	{
    	//printf("\nThe value of i= %d times.\n", i);
    	sleep (1);	
  	}
    y++;
    printf("  --  Signal caught: %d\n", y);
    if (y==5)
    {
    	y=0;
    }
  }
}

void writeFunction(char buf[20]) {
	//strcpy(buf, "This is a test\n");
	nbytes = strlen(buf);
	bytes_written = write(fd, buf, nbytes);
}




