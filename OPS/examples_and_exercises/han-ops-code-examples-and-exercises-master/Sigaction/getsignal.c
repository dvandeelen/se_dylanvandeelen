

#include <stdio.h>   // printf, getchar
#include <string.h>  // memset
#include <signal.h>  // sigaction, sigemptyset, struct sigaction, SIGINT
#include <unistd.h>	//sleep function
#include <sys/types.h>


int main(void) {

  // Count Ctrl-Cs pressed; continue upon Enter:
  printf("Counting Ctrl-Cs until you press Enter...\n");
  int i =0;
  int y =0;
  
  while(1)
  {
  	for (i =0; i < 5; i++)
  	{
    	printf("\nThe value of i= %d times.\n", i);
    	sleep (1);	
  	}
    y++;	 
    printf("\nThe value of y= %d times.\n", y);
    if (y==5)
    {
    	y=0;
    }
  }


  
  
  /*while(getchar() != '\n') {
    i++;
    printf("\nThe value of i= %d times.\n", i);
    //sleep (1);
  }
  */
  //printf("\nCtrl-C has been pressed %d times.  Press Ctrl-C again to exit.\n", i);
  
  
  return 0;
}

