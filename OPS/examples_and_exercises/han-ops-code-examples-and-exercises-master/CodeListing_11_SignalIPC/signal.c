
/*!

 \file signal.c
 \brief Sample code to demonstrate signal between parent and child
 \author Remko Welling (WLGRW) 
 \date 24-3-2019
 \version 1.0
 
 Program description
 ===================
 This program will send a signal from the child to the parent on which the parent 
 will send "Hello World".
 
 Operation
 =========
 Compile the code using "gcc -o signal signal.c -pthread" and run "signal".
 
 Assignment:
 -----------
 1 - Study the code and add comments that explain the program.
 2 - Comment the code describing the operation
 3 - Explain for the class how the program works and why the semaphores do not work.

 */
#define _POSIX_C_SOURCE 199309L  // sigaction(), struct sigaction, sigemptyset()
#include <stdio.h>    // printf, getchar
#include <signal.h>   // sigaction, sigemptyset, struct sigaction, SIGINT
#include <unistd.h>   // fork()
#include <stdlib.h>   // exit() 
#include <sys/wait.h> // wait()

void hello();

int main(){

  pid_t cpid;
  pid_t ppid;
  signal(SIGUSR1, hello);
  if ( (cpid = fork()) == 0) // Child
  {
    ppid = getppid();
    kill(ppid, SIGUSR1);
    exit(0);
  }
  else                       // Parent
  {
    wait(NULL);
  }
}

void hello()
{
  printf("Hello World!\n");
}

