/*!
  \file diningPhilosophersProblem.c
  Original file by: ANUSHKA DESHPANDE
  Adapted for OPS class by Remko Welling.
  \author Remko Welling (remko.welling@han.nl)

  # The Dining Philosopher’s Problem
  For a description see source: https://medium.com/swlh/the-dining-philosophers-problem-bbdb92e6b788

  This code wat taken from: The Dining Philosophers Problem Solution in C:
  See source: https://medium.com/swlh/the-dining-philosophers-problem-solution-in-c-90e2593f64e8

  # Assignments
  Study the code and explain how it works.

  To examine the threads spawn by this program type `ps -eLf | grep dining`.

  ## Question 1
  What is the purpose of semaphore room?

  ## Question 2
  Does this code as presented suffer form deadlock?

  ## Question 3
  How can deadlock being prevented?

*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>
#include <string.h>     // memset
#include <signal.h>     // sigaction, sigemptyset, struct sigaction, SIGINT

// defines to prevent use of magic numbers
#define EXERSIZING      1   /// Running state for threads
#define STOP            0   /// Stop state for threads
#define CHOPSTICK_COUNT 5   /// Number of chopstick available for the philosophers

// Variables
volatile sig_atomic_t running = 1;  /// atomic variable to be used by threads to stop.

sem_t room;                         /// Logistics to manage room
sem_t chopstick[CHOPSTICK_COUNT];   /// Array of semaphores to manage chopsticks

// Function delacarations

/// \brief Philosopher thread function
void * philosopher(void *);

/// \brief "Eat" function
/// \pre spawn thread that may call this function only.
/// \param philosopher Number of teh philosopher that starts eating
void eat(int philosopher);

/// \brief New signal handler
/// \param sig Signal received to be applied to the functions
void newHandler(int sig);           /// Sighandler to capture ctrl-C from keyboard

// main function
int main()
{
  struct sigaction act;     // Struct to store new signal handler
  struct sigaction oldact;  // Struct to store old signal handler that will be replaced.

  // Define SHR:
  memset(&act, '\0', sizeof(act));  // Fill act with NULLs by default
  act.sa_handler = newHandler;      // Set the custom SHR
  act.sa_flags = 0;                 // No flags, used with act.sa_handler
  sigemptyset(&act.sa_mask);        // No signal masking during SHR execution 

  // Install SHR:
  sigaction(SIGINT, &act, &oldact); // SIGINT = Ctrl-C, This cannot be SIGKILL or SIGSTOP

  int i,                            // Counting variable for for-loops
      a[CHOPSTICK_COUNT];           // integer array to pass philosophers ID to thread
  pthread_t tid[CHOPSTICK_COUNT];   // array of thread ID's for house keeping   

  sem_init(&room, 0, 4);            // Initialize semaphore for room

  // Initialize semaphores that protect chopsticks
  for(i=0;i<5;i++){
    sem_init(&chopstick[i], 0, 1);
  }

  // Spawn all threads
  for(i=0; i<5; i++){
    a[i]=i;
    pthread_create(&tid[i], NULL, philosopher, (void *)&a[i]);
  }

  // Join all threads after being ended.
  for(i=0; i<5; i++){
    pthread_join(tid[i], NULL);
  }

  // Restore original SHR:
  sigaction(SIGINT, &oldact, NULL);

  printf("\nPhilosophers problem has ended\n");
}

void * philosopher(void * num)
{
  int philosopher=*(int *)num;
  sem_wait(&room);
  printf("\nPhilosopher %d has entered room", philosopher);

  while(running == EXERSIZING){
    sem_wait(&chopstick[philosopher]);
    printf("\nPhilosopher %d has picked chopstick %d", philosopher, philosopher);
    sem_wait(&chopstick[(philosopher+1)%CHOPSTICK_COUNT]);

    printf("\nPhilosopher %d has picked chopstick %d", philosopher, philosopher+1);

    eat(philosopher);
    // sleep(2);
    printf("\nPhilosopher %d has finished eating", philosopher);

    sem_post(&chopstick[(philosopher+1)%CHOPSTICK_COUNT]);
    printf("\nPhilosopher %d has dropped chopstick %d", philosopher, philosopher+1);

    sem_post(&chopstick[philosopher]);
    printf("\nPhilosopher %d has dropped chopstick %d", philosopher, philosopher);
  }
  printf("\nPhilosopher %d is summend to leave the room", philosopher);

  sem_post(&room);
  printf("\nPhilosopher %d has left the room", philosopher);
}

void eat(int philosopher)
{
  printf("\nPhilosopher %d is eating", philosopher);
  while(running == EXERSIZING);
}

// SHR using sa_handler:
void newHandler(int sig) {
  running = STOP;
}
