/*!

 \file forkedSemaphore.c
 \brief Sample code to demonstrate the correct use of semaphores
 \author Remko Welling (WLGRW) 
 \date 24-3-2019
 \version 1.0
 
 Program description
 ===================
 This program will have a child and parent printing when they start and when they stop to the screen.
 
 The objective is that parent and child will wait for each other and that parent and child will not
 interrupt each other.
 
 +--------------------+
 |KA is uninterrupted:|
 +--------------------+
 |Child start KA      |
 |Child stopt KA      |
 |Parent start KA     |
 |Parent stopt KA     |
 |Child start KA      |
 |Child stopt KA      |
 |Child start KA      |
 |Child stopt KA      |
 |Parent start KA     |
 |Parent stopt KA     |
 |Child start KA      |
 |Child stopt KA      |
 |Child start KA      |
 |Child stopt KA      |
 |Parent start KA     |
 |Parent stopt KA     |
 |Child start KA      |
 |Child stopt KA      |
 |Child start KA      |
 |Child stopt KA      |
 |Parent start KA     |
 |Parent stopt KA     |
 |Child start KA      |
 |enz.                |
 +--------------------+
 
 Operation
 =========
 Compile the code using "make" and run it.
 
 Question: 
 ---------
 Why are parent and child printing waiting for each other.
 
 Assignment:
 -----------
 1 - Study the code and add comments that explain the program.
 2 - Comment the code describing the operation
 3 - Identify the cause why the semaphores are working correctly.
 4 - Explain for the class how the program works and why the semaphores do work.


 */
 
 #include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <wait.h>
#include <time.h>

#define MAXLOOP 10

int main(void)
{
    sem_t *pmutex;
    int i  = 0;
    int iShmID;
    iShmID = shmget(IPC_PRIVATE, sizeof(sem_t), IPC_CREAT | 0600);
    pmutex = shmat(iShmID, NULL, 0);
    
    sem_init(pmutex, 1, 1);

    switch (fork())
    {
    case -1: /*error*/
        printf("Geen child.\n");
        break;
    
    case 0: /*Child process*/
        for (i = 0; i < MAXLOOP; i++)
        {
            sem_wait(pmutex);
            printf("Child start KA\n");
            usleep(10000);
            printf("Child stopt KA\n");
            sem_post(pmutex);
            usleep(13000);
        }
        shmdt(pmutex);
        break;

    default: /*Parent process*/
        for (i = 0; i < MAXLOOP; i++)
        {
            sem_wait(pmutex);
            printf("Parent start KA\n");
            usleep(10000);
            printf("Parent stopt KA\n");
            sem_post(pmutex);
            usleep(30000);
        }
        shmdt(pmutex);
        wait(NULL);
        shmctl(iShmID, IPC_RMID, 0);
        sem_destroy(pmutex);
        break;
    }
    return 0;
}