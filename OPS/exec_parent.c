#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main(void) {
  char text[] = "Greetings from parent!";
  
  switch(fork()) {
  case -1:
    printf("Fork failed.\n");
    exit(1);
    
  case 0:  // Child:
    execl("./exec_child", "exec_child", text, (char *) NULL);
    printf("Starting child failed.\n");
    exit(1);
    
  default:  // Parent:
    printf("Parent: waiting until child finished...\n");
    wait(NULL);
    printf("Parent: child has finished - exiting\n");
  }
  
  return 0;
}
