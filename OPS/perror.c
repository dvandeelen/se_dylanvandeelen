#include <fcntl.h>   // open()
#include <stdio.h>   // perror()
#include <stdlib.h>  // exit()

int main(void) {
  int fd = open("non-existing-file.txt", 0);
  if(fd < 0) {
    perror("non-existing-file.txt");
    exit(1);
  }
}
