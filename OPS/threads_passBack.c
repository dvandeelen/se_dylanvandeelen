// Multithreading, passing the result back using the thread function's argument
#include <pthread.h>
#include <stdio.h>

void *Tenfold(void *arg);  // Thread-function prototype

int main(void) {
  pthread_t thread_id;
  double number = 123.456;          // Local variable on the stack
  
  pthread_create(&thread_id, NULL, Tenfold, (void*) &number);
  
  pthread_join(thread_id, NULL);    // Not receiving anything here
  
  printf("Result: %lf\n", number);
  return 0;
}

void *Tenfold(void *arg) {
  double *result = (double*) arg;   // Cast void* to double*
  *result *= 10;                    // Pass the result back whence it came
  pthread_exit(NULL);               // Not passing anything here
}
