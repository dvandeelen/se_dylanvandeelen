#define _POSIX_C_SOURCE 199309L  // sigaction(), struct sigaction, sigemptyset()
#include <stdio.h>   // printf, getchar
#include <string.h>  // memset
#include <signal.h>  // sigaction, sigemptyset, struct sigaction, SIGINT

volatile sig_atomic_t signalCount = 0;
void newHandler(int sig);

int main(void) {
  struct sigaction act, oldact;
  
  // Define SHR:
  memset(&act, '\0', sizeof(act));  // Fill act with NULLs by default
  act.sa_handler = newHandler;      // Set the custom SHR
  act.sa_flags = 0;                 // No flags, used with act.sa_handler
  sigemptyset(&act.sa_mask);        // No signal masking during SHR execution 
  
  // Install SHR:
  sigaction(SIGINT, &act, &oldact);  // This cannot be SIGKILL or SIGSTOP
  
  // Count Ctrl-Cs pressed; continue upon Enter:
  printf("Counting Ctrl-Cs until you press Enter...\n");
  while(getchar() != '\n') {
    ;
  }
  printf("\nCtrl-C has been pressed %d times.  Press Ctrl-C again to exit.\n", signalCount);
  
  // Restore original SHR:
  sigaction(SIGINT, &oldact, NULL);
  
  // Wait for Ctrl-C to exit:
  while(1);
  
  return 0;
}

// SHR using sa_handler:
void newHandler(int sig) {
  printf("  --  Signal caught: %d\n", sig);
  signalCount++;
}
