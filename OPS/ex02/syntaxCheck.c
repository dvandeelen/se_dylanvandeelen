/******************************************************************************
 * File:         syntaxCheck.c
 * Version:      1.4
 * Datum:        2018-02-20
 * Author:       M. van der Sluys, J.H.L. Onokiewicz, R.B.A. Elsinghorst, J.G. Rouland
 * Description:  OPS exercise 2:  definitions of test functions for display.c
 ******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "syntaxCheck.h"
#include <ctype.h>


// Test whether an argument is one character long and has the correct value (e,p,w):
ErrCode TestType(char *printMethod) {
    ErrCode no_error = NO_ERR;
    ErrCode type_error = ERR_TYPE;
    
    //Check if there's an argument to test
//    printf("char value = %c \n", printMethod[0]);
    int last_pos = strlen(printMethod) -1;
//    printf("int value = %d \n", last_pos);
    
    if (1 > last_pos)
    {
        //compare the last character with the characters '(e,p,w')
        if (0 <= last_pos && 'e' == printMethod[0])
        {
            //printf("the input is 'e'\n");
            return no_error;
        }
        else if (0 <= last_pos && 'p' == printMethod[0])
        {
            //printf("the input is 'p'\n");
            return no_error;
        }
        else if (0 <= last_pos && 'w' == printMethod[0])
        {
            //printf("the input is 'w'\n");
            return no_error;
        }
        else
        {
            return type_error;
        }
    } 
    else
    {
 //       printf("int value = %d \n", last_pos);
        printf("there are to many arguments to test!");
        return type_error;
    }
}  

// Test whether an argument contains a non-negative number:
ErrCode TestNr(char *numberOfTimes) {
    ErrCode no_error = NO_ERR;
    ErrCode number_error = ERR_NR;
    
    int aantal = atoi(numberOfTimes); 
//    printf("aantal = %d \n", aantal);
    
    if (aantal > 0)
    {
        return no_error;
    }
    else
    {
        return  number_error;
    }
}


// Test whether an argument contains only one character:
ErrCode TestChar(char *printChar) {
    ErrCode no_error = NO_ERR;
    ErrCode char_error = ERR_CHAR;
    
    int last_pos = strlen(printChar) -1;
//    printf("int value = %d \n", last_pos);
    
    if (1 > last_pos)
    {
        return  no_error;
    }
    else
    {
        return  char_error;
    }
}
