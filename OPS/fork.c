#include <stdio.h>
#include <unistd.h>

int main(void) {
  
  printf("PID of initial process: %d\n", (int) getpid());
  printf("PPID of initial process: %d (the shell I was started from?)\n",
	 (int) getppid());
  
  // Up to this point, only the initial process exists
  int forkRetVal = fork();
  switch(forkRetVal) {  // Both parent and child carry out switch()
  case -1:  // Initial process:
    printf("Fork failed.\n");
    break;
    
  case 0:   // Child:
    printf("  Child: My PID = %d\n", (int) getpid());
    printf("  Child: My parent's PID = %d\n", (int) getppid());
    break;
    
  default:  // Parent:
    printf("Parent: My PID = %d\n", (int) getpid());
    printf("Parent: My child's PID = %d\n", forkRetVal);
    break;
  }
  
  // From here on, both parent and child run code
  printf("Process with PID %d now stops\n", (int) getpid());
  
  return 0;
}
