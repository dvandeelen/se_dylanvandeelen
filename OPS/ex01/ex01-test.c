/*   getopt_long()
       The following example program illustrates the use of
       getopt_long() with most of its features. */

        #include <stdio.h>     /* for printf */
        #include <stdlib.h>    /* for exit */
        #include <getopt.h>
        #include <string.h>   // str(n)cpy()
        #include <stdbool.h>  // Bool type

        // Function prototypes:
      //  void print_help();
        void read_file(char *fileName, bool lastLine);

       int
       main(int argc, char **argv)
       {
           int c;
           int digit_optind = 0;

           while (1) {
               int this_option_optind = optind ? optind : 1;
               int option_index = 0;
               static struct option long_options[] = {
                   {"add",     required_argument, 0,  0 },
                   {"append",  no_argument,       0,  0 },
                   {"delete",  required_argument, 0,  0 },
                   {"verbose", no_argument,       0,  0 },
                   {"create",  required_argument, 0, 'c'},
                   {"file",    required_argument, 0,  0 },
                   {0,         0,                 0,  0 }
               };

               c = getopt_long(argc, argv, "abc:d:012",
                        long_options, &option_index);
               if (c == -1)
                   break;

               switch (c) {
               case 0:
                    printf("option %s", long_options[option_index].name);
                    printf(" with arg %s", optarg);
                    printf("\n");
                    read_file(optarg, argv);
                   break;

               case '0':
               case '1':
               case '2':
                   if (digit_optind != 0 && digit_optind != this_option_optind)
                     printf("digits occur in two different argv-elements.\n");
                   digit_optind = this_option_optind;
                   printf("option %c\n", c);
                   break;

               case 'a':
                   printf("option a\n");
                   break;

               case 'b':
                   printf("option b\n");
                   break;

               case 'c':
                   printf("option c with value '%s'\n", optarg);
                   break;

               case 'd':
                   printf("option d with value '%s'\n", optarg);
                   break;

               case '?':
                   break;

               default:
                   printf("?? getopt returned character code 0%o ??\n", c);
               }
           }

           if (optind < argc) {
               printf("non-option ARGV-elements: ");
               while (optind < argc)
                   printf("%s ", argv[optind++]);
               printf("\n");
           }

           exit(EXIT_SUCCESS);
       }

       // Read the input file.  lastLine is 0 or 1, depending on whether the first or last line should be printed:
void read_file(char *fileName, bool lastLine) {

   // Verify the file's extension
   char ext[5];
   strncpy(ext, fileName+strlen(fileName)-4,5);  // Get the last 4 characters of the string + \0 !
   if(strcmp(ext,".txt") != 0) {
      fprintf(stderr, "%s:  the input file should be a text file, with the extention '.txt'\n", fileName);
      return;
   }

   FILE *inFile = fopen(fileName, "r");  // NOTE: C stdlib fopen() rather than system call open()
   if( inFile  == NULL) {
      perror(fileName);
      return;
   }

   int iLine = 0;
   char line[1024], firstLine[1024];
   while( fgets(line, 1024, inFile) != NULL ) {
      iLine++;
      if(iLine==1) strncpy(firstLine, line, 1024);  // Save the first line
   }
   fclose(inFile);

   if(lastLine) {
      printf("The last line of the file %s reads:\n%s\n", fileName, line);
   } else {
      printf("The first line of the file %s reads:\n%s\n", fileName, firstLine);
   }
}

