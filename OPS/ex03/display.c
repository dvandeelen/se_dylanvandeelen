/**********
 * File:         display.c
 * Version:      1.4
 * Date:         2018-02-20
 * Author:       M. van der Sluys, J.H.L. Onokiewicz, R.B.A. Elsinghorst, J.G. Rouland
 * Description:  OPS exercise 2: syntax check
 **********/

#include <stdio.h>
#include <stdlib.h>
#include "displayFunctions.h"
#include <unistd.h>
#include <sys/wait.h>
#include <sys/resource.h>

int main(int argc, char *argv[]) {
    unsigned long int numOfTimes, niceIncr;
    char printMethod, printChar;
    ErrCode err;
    
    err = SyntaxCheck(argc, argv);  // Check the command-line parameters
    if(err != NO_ERR) 
    {
        DisplayError(err);        // Print an error message
    } 
    else
    {
        printMethod = argv[1][0];
        numOfTimes = strtoul(argv[2], NULL, 10);  // String to unsigned long
        niceIncr = strtoul(argv[3], NULL, 10);
        int numOfChars = argc - 4;	//Number of inserted characters
        
        printf("Number of characters: %d\n", numOfChars);
        printf("Nice increment: %ld\n", niceIncr);
        
        for (int iChild = 0; iChild < numOfChars; iChild ++)
        {
            int childPriority = iChild * niceIncr;
            printChar = *argv[4+iChild];
            
            int pid = fork();
            
            if (pid == 0)
            {
                setpriority(PRIO_PROCESS, 0, childPriority);
                printf("Child priority: %d, Char %c\n", getpriority(PRIO_PROCESS,0), *argv[4+iChild]);
                PrintCharacters(printMethod, numOfTimes, printChar);
                exit(0); //to exit this child
            }
            printf("Caracter: %c, PID: %d, iChild: %d * niceIncr: %ld = %d\n", *argv[4+iChild], pid, iChild, niceIncr, childPriority);
        }
        int WaitForChild = 0;        
        for (int iChild = 0; iChild < numOfChars; iChild++)
        {
            //printf("Wait before child\n");
            wait(&WaitForChild);
            //printf("Wait afer child\n");
        }
    }
    printf("\nFinally, All childs leave this home ;)!\n");
    return 0;
}
