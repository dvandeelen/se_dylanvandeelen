#include <sys/wait.h>  // wait()
#include <stdio.h>     // perror()
#include <stdlib.h>    // exit()
#include <unistd.h>    // pipe(),dup*(), fork(),exec*(), read(),write(),close()

int main(void) {
  int pipeFD[2];
  char buf;
  
  // Create pipe:
  if(pipe(pipeFD) == -1) {
    perror("pipe");
    exit(EXIT_FAILURE);
  }
  
  // Fork off child process:
  switch(fork()) {
  case -1:
    perror("fork");
    exit(EXIT_FAILURE);
    
  case 0:    // Child - writes output of ls to pipe:
    
    close(pipeFD[0]);          // Close read end - not necessary, but unused
    
    // close(1);               // Close stdout - ls output not sent to stdout
    // dup(pipeFD[1]);         // Dup pipe write end - should take FD 1 (stdout)
    dup2(pipeFD[1], 1);        // Close(1) and dup(pipeFD[1]) to FD 1 in 1 call
    close(pipeFD[1]);          // Only stdOut (FD 1) open, redir to pipeFD[0]
    
    execlp("ls", "ls", NULL);  // Starts ls and should never return
    perror("execlp");
    exit(EXIT_FAILURE);
    
  default:           // Parent - reads from pipe and writes to stdOut:
    
    // We MUST close the unused write end, since the read() call suspends the
    // parent when the pipe is empty (and a write end still exists).  The other
    // write end disappears as soon as ls exits, since the process terminates.
    close(pipeFD[1]);  // Try commenting this out
    
    // Read from pipe and write to stdout:
    while(read(pipeFD[0], &buf, 1) > 0)  write(1, &buf, 1);
    
    wait(NULL);                // Wait for child
    close(pipeFD[0]);          // Tidy up
    
    exit(EXIT_SUCCESS);
  }
  
}
