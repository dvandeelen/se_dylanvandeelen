#define _POSIX_C_SOURCE 199309L
#include <stdio.h>	// printf, getchar
#include <stdlib.h>
#include <unistd.h>	// sleep, write
#include <signal.h>	// sigaction, sigemptyset, struct sigaction, SIGINT
#include <string.h>	// memset
#include <sys/types.h>

char counter = '0';
void newHandler(int sig, siginfo_t *info, void *ucontext);

int main() {
  struct sigaction act, oldact;

  memset(&act, '\0', sizeof(act));
  act.sa_sigaction = newHandler;
  act.sa_flags = SA_SIGINFO;
  sigemptyset(&act.sa_mask);
  sigaction(SIGXFSZ, &act, &oldact);
  
  printf("PID = %i\n", getpid());

  while(1){
    if(counter > '9'){
      counter = '0';
    }
    write(1, &counter, 1);
    sleep(1);
  }
  sigaction(SIGXFSZ, &oldact, NULL);
  return 0;
}

void newHandler(int sig, siginfo_t *info, void *ucontect){
  counter++;
}
